package 'nginx' do
    action :install
  end
  
  file '/etc/nginx/sites-available/default' do
    content IO.read('/home/ubuntu/nginx.tpl')
    action :create
  end
  
  service 'nginx' do
    action :start
  end
  
  service 'nginx' do
    action :restart
  end
