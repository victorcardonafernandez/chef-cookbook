package 'nginx' do
  action :install
end

file '/var/www/html/index.html' do
  content "<title>nginx in Private Instance</title><h1>nginx deployed via chef cookbook in a private instance. Accessing using a reverse proxy</h1>"
  action :create
end

service 'nginx' do
  action :start
end

service 'nginx' do
  action :restart
end
